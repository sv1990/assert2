#ifndef DECOMPOSE_ASSERT_HH_1540294889442069645_
#define DECOMPOSE_ASSERT_HH_1540294889442069645_

#include <functional>
#include <iostream>
#include <type_traits>

namespace assertions { namespace detail {

// Since operator<< has a higher precedence than e.g. operator< one can
// decompose an expression like a < b  by prepending decomposer{}<<.

struct decomposer {};

template <typename T>
struct unary_predicate {
  const T& value;
  constexpr operator bool() const noexcept { return value; }
};

template <typename T>
std::ostream& operator<<(std::ostream& os,
                         const unary_predicate<T>& l) noexcept {
  return os << l.value;
}

template <typename T>
constexpr unary_predicate<T> operator<<(decomposer, const T& x) noexcept {
  return {x};
}


template <typename T>
struct delay_false : std::false_type {};


template <typename T>
struct rep {
  static_assert(delay_false<T>::value, "Operator not supported");
};

#define MAKE_REP(pred, op)                                                     \
  template <typename T>                                                        \
  struct rep<std::pred<T>> {                                                   \
    static inline const char* value = #op;                                     \
  };

MAKE_REP(less, <)
MAKE_REP(less_equal, <=)
MAKE_REP(greater, >)
MAKE_REP(greater_equal, >=)
MAKE_REP(equal_to, ==)
MAKE_REP(not_equal_to, !=)

#undef MAKE_REP

template <typename L, typename R, typename Pred>
struct binary_predicate {
  const L& lhs;
  const R& rhs;
  Pred pred;
  constexpr operator bool() const noexcept { return pred(lhs, rhs); }
};

template <typename L, typename R, typename Pred>
std::ostream& operator<<(std::ostream& os,
                         const binary_predicate<L, R, Pred>& dec) noexcept {
  return os << dec.lhs << ' ' << rep<Pred>::value << ' ' << dec.rhs;
}

#define MAKE_BINARY_PREDICATE(pred, op)                                        \
  template <typename L, typename R>                                            \
  constexpr binary_predicate<L, R, std::pred<std::common_type_t<L, R>>>        \
  operator op(const unary_predicate<L>& l, const R& r) noexcept {              \
    return {l.value, r, std::pred<std::common_type_t<L, R>>{}};                \
  }

MAKE_BINARY_PREDICATE(equal_to, ==)
MAKE_BINARY_PREDICATE(not_equal_to, !=)
MAKE_BINARY_PREDICATE(greater, >)
MAKE_BINARY_PREDICATE(greater_equal, >=)
MAKE_BINARY_PREDICATE(less, <)
MAKE_BINARY_PREDICATE(less_equal, <=)

#undef MAKE_BINARY_PREDICATE

#define MAKE_COMP_TESTER(op)                                                   \
  template <typename T>                                                        \
  auto op##_tester(std::op<T>) {}                                              \
  template <typename P>                                                        \
  auto is_##op##_detail(P p, int)->decltype(op##_tester(p), std::true_type{}); \
  template <typename P>                                                        \
  std::false_type is_##op##_detail(P, long);                                   \
  template <typename P>                                                        \
  using is_##op = decltype(is_##op##_detail(P{}, 0));

MAKE_COMP_TESTER(less);
MAKE_COMP_TESTER(less_equal);
MAKE_COMP_TESTER(greater);
MAKE_COMP_TESTER(greater_equal);
#undef MAKE_COMP_TESTER

template <typename P>
constexpr bool is_less_type = is_less<P>::value || is_less_equal<P>::value;

template <typename P>
constexpr bool is_greater_type =
    is_greater<P>::value || is_greater_equal<P>::value;

template <typename L, typename M, typename R, typename Pred1, typename Pred2>
struct ternary_predicate {
  static_assert((is_less_type<Pred1> && is_less_type<Pred2>) ||
                    (is_greater_type<Pred1> && is_less_type<Pred2>),
                "The comparisons in a ternary comparison should point in the "
                "same direction");
  const L& left;
  const M& middle;
  const R& right;
  Pred1 pred1;
  Pred2 pred2;
  constexpr operator bool() const noexcept {
    return pred1(left, middle) && pred2(middle, right);
  }
};

template <typename L, typename M, typename R, typename Pred1, typename Pred2>
std::ostream&
operator<<(std::ostream& os,
           const ternary_predicate<L, M, R, Pred1, Pred2>& pred) noexcept {
  return os << pred.left << ' ' << rep<Pred1>::value << ' ' << pred.middle
            << ' ' << rep<Pred2>::value << ' ' << pred.right;
}

#define MAKE_TERNARY_PREDICATE(pred2, op2)                                     \
  template <typename L, typename M, typename R, typename Pred1>                \
  constexpr ternary_predicate<L, M, R, Pred1,                                  \
                              std::pred2<std::common_type_t<M, R>>>            \
  operator op2(const binary_predicate<L, M, Pred1>& l, const R& r) noexcept {  \
    return {l.lhs, l.rhs, r, l.pred, std::pred2<std::common_type_t<M, R>>{}};  \
  }

MAKE_TERNARY_PREDICATE(equal_to, ==)
MAKE_TERNARY_PREDICATE(not_equal_to, !=)
MAKE_TERNARY_PREDICATE(greater, >)
MAKE_TERNARY_PREDICATE(greater_equal, >=)
MAKE_TERNARY_PREDICATE(less, <)
MAKE_TERNARY_PREDICATE(less_equal, <=)

#undef MAKE_TERNARY_PREDICATE

#define FORBID_OPERATOR(op)                                                    \
  template <typename L, typename M, typename R, typename Pred1,                \
            typename Pred2, typename T>                                        \
  auto operator op(ternary_predicate<L, M, R, Pred1, Pred2>, T) {              \
    static_assert(delay_false<T>::value, "Expression not supported!");         \
  }                                                                            \
  template <typename L, typename M, typename R, typename Pred1,                \
            typename Pred2, typename T>                                        \
  auto operator op(T, ternary_predicate<L, M, R, Pred1, Pred2>) {              \
    static_assert(delay_false<T>::value, "Expression not supported!");         \
  }

// These operators wont have the correct meaning
FORBID_OPERATOR(>)
FORBID_OPERATOR(>=)
FORBID_OPERATOR(<)
FORBID_OPERATOR(<=)

#undef FORBID_OPERATOR


template <typename Predicate>
void explain_assertion(std::ostream& os, const Predicate& pred) noexcept {
  os << "since " << pred << " is false\n";
}

template <typename T>
void explain_assertion(std::ostream&, const unary_predicate<T>&) noexcept {
  // Nothing to print
}

inline void explain_assertion(std::ostream&, bool) noexcept {
  // Nothing to print
}

template <typename Predicate>
inline void assertion_failed([[maybe_unused]] const char* expr,
                             [[maybe_unused]] const Predicate& pred,
                             [[maybe_unused]] const char* file,
                             [[maybe_unused]] int line,
                             [[maybe_unused]] const char* func) noexcept {
  std::cerr << file << ':' << line << ": " << func << ":\n"
            << "Assertion '" << expr << "' failed\n";
  explain_assertion(std::cerr, pred);
#ifndef ASSERT_NO_ABORT
  std::abort();
#endif
}

template <typename Predicate>
inline void assertion([[maybe_unused]] const char* expr, //
                      [[maybe_unused]] Predicate&& pred, //
                      [[maybe_unused]] const char* file, //
                      [[maybe_unused]] int line,         //
                      [[maybe_unused]] const char* func) noexcept {
#ifndef NDEBUG
  if (!pred) {
    assertion_failed(expr, std::forward<Predicate>(pred), file, line, func);
  }
#endif
}


}} // namespace assertions::detail

#ifndef ASSERT_DISABLE_DECOMPOSE
#  define ASSERT_DECOMPOSE(expr) (assertions::detail::decomposer{} << expr)
#else
#  define ASSERT_DECOMPOSE(expr) (expr)
#endif

#define DO_PRAGMA(x) _Pragma(#x)

#ifdef __clang__
#  define CLANG_OP_WARNING_PUSH()                                              \
    DO_PRAGMA(clang diagnostic push)                                           \
    DO_PRAGMA(clang diagnostic ignored "-Woverloaded-shift-op-parentheses")
#  define CLANG_OP_WARNING_POP() DO_PRAGMA(clang diagnostic pop)
#else
#  define CLANG_OP_WARNING_PUSH()
#  define CLANG_OP_WARNING_POP()
#endif

// clang-format off
#define ASSERT(expr)                                                           \
  CLANG_OP_WARNING_PUSH()                                                      \
  assertions::detail::assertion(#expr, ASSERT_DECOMPOSE(expr), __FILE__,       \
                                __LINE__, __PRETTY_FUNCTION__)                 \
  CLANG_OP_WARNING_POP()
// clang-format on

#endif // DECOMPOSE_ASSERT_HH_1540294889442069645_
