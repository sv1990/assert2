#define ASSERT_NO_ABORT
#include "assert.hh"

#include <algorithm>
#include <vector>

int main() {
  std::vector<int> v{1, 3, 2};
  ASSERT(std::is_sorted(begin(v), end(v)));
  std::cerr << '\n';
  std::size_t a = 0, b = 5;
  ASSERT(a <= b < v.size());

  int i = 5, n = 3;
  ASSERT(i >= 0 && i < n); // && not supported. Falls back to simple bool case
}
